<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <!-- Links -->
        <link rel="stylesheet" href="{{ asset('css/style.css') }}" />
        <link rel="stylesheet" type="text/css" href="https://file.myfontastic.com/gEpYKGo5aJ95CbiZbSXwZ7/icons.css">
        <link rel="stylesheet" href="{{ asset('bootstrap/css/bootstrap.min.css') }}">

        <title>Portafolio</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">

    </head>
    <body>
        <header>
            <div class="aboutContainer">
                <div class="aboutCard">
                    <div class="card">
                        <div id="divImgCard">
                            <img id="IDiana" src="images/9.jpg" alt="No se pudo cargar la imágen" style="width: 74.5%">
                        </div>
                        <h1 id="nameCard">Diana Marcela Solano Ostos</h1>
                        <table class="tableCard">
                            <tr>
                                <td class="tdRight">Cargo:</td>
                                <td class="tdLeft">Estudiante del <b>SENA</td>
                            </tr>
                            <tr>
                                <td class="tdRight">Fecha de nacimiento:</td>
                                <td class="tdLeft">11 de enero de 2000</td>
                            </tr>
                            <tr>
                                <td class="tdRight">Sexo:</td>
                                <td class="tdLeft">Femenino</td>
                            </tr>
                            <tr>
                                <td class="tdRight">Celular:</td>
                                <td class="tdLeft">3214342027</td>
                            </tr>
                        </table>
                        <div id="divCard">
                            <a href="https://www.facebook.com/diana.solano.52687" aria-hidden="true" class="social-facebook-square aCard"></a>
                            <a href="https://github.com/DianaSolano1" aria-hidden="true" class="social-github aCard"></a>
                            <a href="https://plus.google.com/110641920506624820080" aria-hidden="true" class="social-google-plus-square aCard"></a>
                            <a href="https://www.youtube.com/channel/UCxhtz7jLbG4UmMP41ub4dQw" aria-hidden="true" class="social-youtube aCard"></a>
                            <p>Redes Sociales</p>
                        </div>
                    </div>
                </div>
                <div class="aboutCard">
                    <div id="about">
                        <div id="divAbout">
                            <h2 id="h2About">Sobre mí</h2>
                        </div>
                        <div class="aboutContent">
                            <p id="principalAbout">
                                Mi nombre es <b>Diana Marcela Solano Ostos</b>, nací el 11 de enero del 2000. Realicé mi primaria y secundaria en el colegio Gerardo Paredes (2005- 2016), realicé un técnico en <b>desarrollo de software</b> en la institución educativa Sena durante mi etapa de preparatoria (2015 - 2016). Actualmente estoy realizando mi tecnólogo en <b>análisis y desarrollo de sistemas de información (ADSI)</b> en esta misma institución
                            </p>
                            <p id="secundaryAbout">
                                Presento conocimientos en las tecnologías c#, html, css, java, JavaScript, php, jsp, PostgresSQL y mySql, además de esto, estoy aprendiendo Laravel, Sass, hibernate, Python, vue.js, Bootstrap y Oracle DB.
                                Actualmente me encuentro desarrollando un taller de implementación en el Sena, en el cual se seleccionaron a los aprendices más capacitados para desarrollar e implementar proyectos a gran escala para la institución.
                            </p>
                        </div>
                        <h4 id="h4About">Bienvenid@, aquí encontrarás toda mi vida laboral</h4>
                    </div>
                </div>
            </div>
            <div id="navbar">
                <div id="navbarSecundary">
                    <a href="#tab1" class="navbarActive" data-target="tab1">
                        <span class="glyphicon glyphicon-home"></span>
                        <span>Linea de tiempo</span>
                    </a>
                    <a href="#tab2" data-target="tab2">
                        <span class="glyphicon glyphicon-briefcase"></span>
                        <span>Portafolio</span>
                    </a>
                    <a href="#tab3" data-target="tab3">
                        <span class="glyphicon glyphicon-folder-close"></span>
                        <span>Proyectos</span>
                    </a>
                    <a href="#">
                        <span class="glyphicon glyphicon-envelope"></span>
                        <span>Contáctame</span>
                    </a>
                    <a href="#">
                        <span class="glyphicon glyphicon-user"></span>
                        <span>Inicia Sesión</span>
                    </a>
                </div>
            </div>
        </header>
        <main>
            <div class="tabContentContainer">
                <div class="tabSlide" id="tab1">
                    <h1>Linea de tiempo</h1>
                    <p>Aquí se presentará mi linea de tiempo, en donde expondré los eventos más relevantes de mi vida, incluyendo vida estudiantil y laboral.</p>
                    <iframe src='https://cdn.knightlab.com/libs/timeline3/latest/embed/index.html?source=1VEdES7ZG6pTzMwC2345KbvZ3UPYDZr6b8jhAwtTwPRU&font=Default&lang=en&initial_zoom=2&height=550' width='98%' height='550' webkitallowfullscreen mozallowfullscreen allowfullscreen frameborder='0'></iframe>
                    <div class="timeLine">
                        
                    </div>
                    <span class="glyphicon glyphicon-remove xButton"></span>
                </div>
                <div class="tabSlide" id="tab2">
                    <h1>Portafolio</h1>
                    <p>Aquí se presentará mi portafolio, en donde se dividirá por hoja de vida, documentos, trabajos y mini-proyectos.</p>
                    <div class="containerImg">
                        div*2>img+p>table>tr*5>td{:}+td

                        <div class="divImg">
                            <img src="images/9.jpg" alt="No se pudo cargar la imágen" class="imgPortfolio">
                            <p></p>
                        </div>
                        <div class="divImg">
                            <img src="images/9.jpg" alt="No se pudo cargar la imágen" class="imgPortfolio">
                            <p>Hola</p>
                        </div>
                        <div>
                            <img src="images/9.jpg" alt="No se pudo cargar la imágen" class="imgPortfolio">
                            <p>Hola</p>
                        </div>
                        <div>
                            <img src="images/9.jpg" alt="No se pudo cargar la imágen" class="imgPortfolio">
                            <p>Hola</p>
                        </div>
                        <div>
                            <img src="images/9.jpg" alt="No se pudo cargar la imágen" class="imgPortfolio">
                            <p>Hola</p>
                        </div>
                        <div>
                            <img src="images/9.jpg" alt="No se pudo cargar la imágen" class="imgPortfolio">
                            <p>Hola</p>
                        </div>
                        <div>
                            <img src="images/9.jpg" alt="No se pudo cargar la imágen" class="imgPortfolio">
                            <p>Hola</p>
                        </div>
                    </div>
                    <span class="glyphicon glyphicon-remove xButton"></span>
                </div>
                <div class="tabSlide" id="tab3">
                    <h1>Proyectos</h1>
                    <p>Aquí presentaré más a fondo mis proyectos, teniendo en cuenta el año de inicio y finalización de estos. Cabe aclarar, que sólo son proyectos, y no contiene más información de trabajos o mini-trabajos.</p>
                    <span class="glyphicon glyphicon-remove xButton"></span>
                </div>

            </div>
        </main>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
        <script src="{{ asset('js/script.js') }}"></script>
        <script src="{{ asset('js/tabs.js') }}"></script>
        <script src="{{ asset('bootstrap/js/bootstrap.min.js') }}"></script>
    </body>
</html>
