<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAlterAllTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function($table){
            $table->foreign('roles_id')
                ->references('id')
                ->on('roles')
                ->onUpdate('cascade');
        });

        Schema::table('informations', function($table){
            $table->foreign('typeInformations_id')
                ->references('id')
                ->on('typeInformations')
                ->onUpdate('cascade');

            $table->foreign('users_id')
                ->references('id')
                ->on('users')
                ->onUpdate('cascade');
        });

        Schema::table('links', function($table){
            $table->foreign('users_id')
                ->references('id')
                ->on('users')
                ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
