<?php

namespace Models/App;

use Illuminate\Database\Eloquent\Model;

class Information extends Model
{
    protected $table = 'informations';
    protected $fillable = ['name','description','creationDate','duration','typeInformations_id'];
    protected $guarded = ['id'];

    public function typeInformations(){
    	return $this->belongsToMany(TypeInformation::class);
    }
    public function users(){
    	return $this->hasMany(User::class);
    }
}
