<?php

namespace Models/App;

use Illuminate\Database\Eloquent\Model;

class Rol extends Model
{
    protected $table = 'roles';
    protected $fillable = ['name','description'];
    protected $guarded = ['id'];

    public function user(){
    	return $this->hasOne(User::class);
    }
}
