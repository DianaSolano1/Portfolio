<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Link extends Model
{
    protected $table = 'links';
    protected $fillable = ['link'];
    protected $guarded = ['id'];

    public function user(){
    	return $this->hasOne(User::class);
    }
}
