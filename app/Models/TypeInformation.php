<?php

namespace Models/App;

use Illuminate\Database\Eloquent\Model;

class TypeInformation extends Model
{
    protected $table = 'typeInformations';
    protected $fillable = ['name','description'];
    protected $guarded = ['id'];

    public function informations(){
    	return $this->belongsToMany(Information::class);
    }
}
