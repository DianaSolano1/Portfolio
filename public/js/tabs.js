var tabContainer = $('#navbarSecundary')
var superTab = $('#navbar')

tabContainer.on('click', 'a', function(){

	superTab.addClass('borderBottom')

	var link = $(this)
	var target = link.data('target')

	var tabSlide = $(`#${target}`)
	tabSlide.siblings().fadeOut(function() {
		tabSlide.fadeIn()	
	})
	

	$(this).addClass('navbarActive').siblings().removeClass('navbarActive')

})

$('.xButton').on('click', function(){

	$(this).parents('.tabSlide').fadeOut()

})